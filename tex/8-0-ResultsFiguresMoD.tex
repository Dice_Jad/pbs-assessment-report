\chapter{Detailed Results}\label{chapter:DetailedResults}

	The results for each simulated manoeuvre for the vehicle are discussed in the sections below. A summary of the performance achieved for each of the standards is given in Table \ref{table:Results}.

	\section{Yaw Damping Coefficient}\label{section:YawD}

		The \gls{ydc} gives a measure of the rate at which “snaking” or yaw oscillations decay after a severe steering input at high speed. Figure \ref{figure:YawD} shows the yaw rate response of the vehicle unit to a short-duration sinusoidal steer input. The minimum \gls{ydc} requirement is 0.15 according to the Australian \gls{pbs} assessment rules (National Transport Commission, 2008).
		
		\putfigH{1}{fig/YawD}{Yaw Damping Coefficient}{figure:YawD}\newpage
	
	\section{Static Rollover Threshold}\label{section:SRT}

		The \gls{srt} test determines the maximum steady state lateral acceleration that can be sustained in a constant radius high speed turn. This directly measures the vehicle’s rollover stability. The lateral accelerations of the individual units are combined as described in the \gls{pbs} assessment rules (National Transport Commission, 2008) to give the lateral acceleration of each \gls{rcu}. Figure \ref{figure:SRT} illustrates the \gls{srt} performance using a tilt table. The minimum \gls{srt} requirement is 0.40 for road tankers hauling dangerous goods in bulk and buses and coaches and 0.35 for all other vehicles according to the Australian \gls{pbs} assessment rules (National Transport Commission, 2008).
		
		\putfigH{1}{fig/SRTt_time}{Static Rollover Threshold}{figure:SRT}\newpage

	\section{High-Speed Lane Change}\label{section:HSTO}

		The lateral acceleration experienced by each unit in a vehicle combination is amplified in comparison with that of the unit immediately ahead of it as the vehicle changes course at high-speeds. This behaviour is evaluated using a single lane change manoeuvre at a speed of 88 km/h. The lateral accelerations of the individual units in the \gls{rrcu} are combined as described in the \gls{pbs} assessment rules (National Transport Commission, 2008) as shown in Figure \ref{figure:RA}. The \gls{ra} is the ratio of the maximum lateral accelerations of the \gls{rrcu} and the steer axle.
		
		\putfigH{1}{fig/RA}{Rearward Amplification of the Vehicle}{figure:RA}\newpage

		The \gls{hsto} performance limits the amount of overshoot of the rearmost axle of the vehicle during the same manoeuvre. The \gls{hsto} performance is shown in Figure \ref{figure:HSTO}.
		
		\putfigH{1}{fig/HSTO}{High-speed Transient Offtracking}{figure:HSTO}\newpage

	\section{Tracking Ability on a Straight Path}\label{section:TASP}
		The \gls{tasp} performance measure is used to ensure that the vehicle will remain within its lane when travelling on roads with rough surfaces. The measure is evaluated by determining the maximum width of road used by the vehicle as it traverses a 1000 m long section of road with a defined roughness and cross-slope. The red lines in Figure \ref{figure:TASP} show the leftmost and rightmost points on the vehicle as it moves along the test section. Note that the simulation has 200 m lead-in and lead-out sections.
		
		\putfigH{1}{fig/TASP}{Tracking Ability on a Straight Path}{figure:TASP}\newpage

	\section{Low-Speed Turn}\label{section:LST}
		The low-speed turn manoeuvre is a 90\degree{} turn with a 12.5 m outer radius and is executed at a speed of 5 km/h. The manoeuvre is used to determine the road space used by the vehicle at low speeds. The manoeuvre was conducted in the laden and unladen state. Figure \ref{figure:LSSPladen} and Figure \ref{figure:LSSPunladen} shows the path followed by the front outer corner of the prime mover and the innermost point of the vehicle during the turn for the laden and unladen vehicles respectively. The red line indicates the maximum swept path width. This is a straight line that intersects both paths perpendicular to their tangents.
		
		\putfigH{1}{fig/LSSP_laden}{Low-speed Swept Path - Laden}{figure:LSSPladen}

		\putfigH{1}{fig/LSSP_unladen}{Low-speed Swept Path - Unladen}{figure:LSSPunladen}

		The \gls{fs}, \gls{mod} and \gls{dom} standards are evaluated from the same manoeuvre and are intended to limit the amount of swing-out of the front corners of each vehicle unit. \gls{fs} (See Figure \ref{figure:FSladen} and Figure \ref{figure:FSunladen}) is the maximum swept path width between the outer wall of the front steer tyre and the front outer corner of the prime mover. \gls{mod} is evaluated by identifying the largest difference in frontal swing-out between adjacent units whereas \gls{dom} is the largest of the difference in maximum frontal swing-out values between adjacent units. \gls{mod} and \gls{dom} are only required for vehicle configurations with semitrailers and are shown in Figures \ref{figure:MoDladen} to \ref{figure:DoMladen}.

		\putfigH{1}{fig/FS_laden}{Frontal Swing - Laden}{figure:FSladen}

		\putfigH{1}{fig/FS_unladen}{Frontal Swing - Unladen}{figure:FSunladen}

		\putfigH{1}{fig/MoD_laden}{Maximum of Difference - Laden}{figure:MoDladen}

		\putfigH{1}{fig/MoD_unladen}{Maximum of Difference - Unladen}{figure:MoDunladen}

		\putfigH{1}{fig/DoM_laden}{Difference of Maxima - Laden}{figure:DoMladen}

		\putfigH{1}{fig/DoM_unladen}{Difference of Maxima - Unladen}{figure:DoMunladen}\newpage

		The \gls{ts} measures the maximum swing-out of the rear corner of each vehicle unit at the commencement and end of the low-speed turn manoeuvre (See Figure \ref{figure:TSladen} and Figure \ref{figure:TSunladen}). The rigorous interpretation of the Australian \gls{pbs} assessment rules (National Transport Commission, 2008) requires the tail swing to be evaluated from the entry tangent of the reference point in question. The less severe interpretation is that \gls{ts} is evaluated as the swing out from the maximum vehicle width.

		\putfigH{1}{fig/TS_laden}{Tail Swing - Laden}{figure:TSladen}

		\putfigH{1}{fig/TS_unladen}{Tail Swing - Unladen}{figure:TSunladen}\newpage

		\gls{stfd} measures the proportion of the available tyre to road friction that is used by the vehicle’s steer axle(s) when performing the \gls{lssp} 90\degree turn (See Figure \ref{figure:STFDladen} and Figure \ref{figure:STFDunladen}). If the horizontal force required at the steer axle(s) to keep the vehicle moving in the intended direction exceeds that available from the friction at the contact with the road, the vehicle will fail to turn sharply enough and may move out of its lane.

		\putfigH{1}{fig/STFD_laden}{Steer-Tyre Friction Demand - Laden}{figure:STFDladen}

		\putfigH{1}{fig/STFD_unladen}{Steer-Tyre Friction Demand - Unladen}{figure:STFDunladen}\newpage

	\section{Longitudinal Performance}\label{section:LongitudinalPerformance}
		\textbf{\gls{sta}} measures the maximum gradient on which the vehicle can accelerate from rest (See Figure \ref{figure:START}).\\ 
		\textbf{\gls{graa}} measures the maximum gradient on which the vehicle can maintain speed (See Figure \ref{figure:GRAa}).\\ 
		\textbf{\gls{grab}} measures the maximum speed which the vehicle can maintain on a 1\% gradient (See Figure \ref{figure:GRAb}).\\ 
		\textbf{\gls{acc}} measures the time taken to travel a distance of 100 m on a flat road (See Figure \ref{figure:ACC}).	
			
		\putfigH{1}{fig/START}{Startability of the Vehicle}{figure:START}

		\putfigH{1}{fig/GRADA}{Gradeability A of the Vehicle}{figure:GRAa}

		\putfigH{1}{fig/GRADB}{Gradeability B of the Vehicle}{figure:GRAb}

		\putfigH{1}{fig/ACC}{Accelerability}{figure:ACC}